package com.example.aop_part5_chapter02.utility

import com.google.firebase.crashlytics.FirebaseCrashlytics
import java.lang.Exception

private val crashlytics by lazy {
    FirebaseCrashlytics.getInstance()
}

object CrashlyticsUtil {

    private const val LAST_UI_ACTION = "last_ui_action"

    fun setLastUIAction(action: String) {
        crashlytics.setCustomKey(LAST_UI_ACTION, action)
    }

    fun setUser(id: String) {
        crashlytics.setUserId(id)
    }

    fun log(message: String) {
        crashlytics.log(message)
    }

    fun recordException(e: Exception) {
        crashlytics.recordException(e)
    }

}